// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/oleg.kuleba/goBoltDB/db"
)

// findByNumberCmd represents the findByNumber command
var findByNumberCmd = &cobra.Command{
	Use:   "findByNumber",
	Short: "findByNumber phoneNumber",
	Long: `Команда ищет и выводит на экран контакт с указанным номером телефона. Требует передачу одного аргумента - номера телефона
Например: deleteContact +380501234567`,
	Args: cobra.MinimumNArgs(1),
	Run: findByNumber,
}

func findByNumber(cmd *cobra.Command, args []string) {
	/*if !utils.IsFileExist() { // Если файла еще нет, выводим инфу об этом и о том, что создание файла происходит при записи нового контакта.
		return
	}*/
	if !db.Validate(args[0], db.PhoneFlag) { // Если аргумент не проходит валидацию, выводим инфу об этом и выходим
		db.PrintValidationMessages()
		return
	}

	db.FindByNumber(args[0])
}

func init() {
	rootCmd.AddCommand(findByNumberCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// findByNumberCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// findByNumberCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
