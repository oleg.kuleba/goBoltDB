package db

import (
	"fmt"

	"regexp"
	"gitlab.com/oleg.kuleba/goBoltDB/models"
	"github.com/boltdb/bolt"
	"time"
	"github.com/golang/protobuf/proto"
)

// Флаги, использующиеся для валидации
const DBName string = "phoneBook.db"
const ContactsBucketName string = "contacts"
const PhoneFlag string = "phone"
const NameFlag string = "name"
const CityFlag string = "city"
const StreetFlag string = "street"
const BuildingOrApartmentFlag string = "buildingOrApartment"

var PhoneRegexp *regexp.Regexp = regexp.MustCompile("^[+]380([0-9]{9})$")
var NameRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{3,15}$")
var CityRegexp *regexp.Regexp = regexp.MustCompile("^[A-Za-z]{3,15}$")
var StreetRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{4,20}$")
var BuildingOrApartmentRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{1,5}$")

func AddContact(number string, contact *models.Contact) bool {
	var flagToReturn bool
	// Открываем БД
	dbContacts, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer dbContacts.Close()

	// Открываем Writable транзакцию
	if err := dbContacts.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(ContactsBucketName))
		Check(err)

		if bucket.Get([]byte(number)) != nil { // Проверяем наличие номера в БД (для обеспечения уникальности). Если есть - сообщаем об этом и выходим
			fmt.Println("Запись с таким номером уже существует. Для изменения данных используйте команду editContact")
		} else { // Если номера в БД нет - добавляем
			out, err := proto.Marshal(contact)
			Check(err)
			err = bucket.Put([]byte(number), out) //[]byte(contact)
			Check(err)
			flagToReturn = true
		}
		return nil
	}); err != nil {
		panic(err)
	}
	return flagToReturn
}

func FindAll() {
	// Открываем БД
	dbContacts, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer dbContacts.Close()

	// Открываем Read-only транзакцию
	if err := dbContacts.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(ContactsBucketName))
		var cursor *bolt.Cursor

		if bucket == nil { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			printEmptyBucketMessage()
		} else {

			cursor = bucket.Cursor()
			if k, _ := cursor.First(); k == nil { // Если cursor баккета пустой (ключей/записей нет) - сообщаем об этом и выходим
				printEmptyBucketMessage()
				return nil
			}
			contact := &models.Contact{}
			fmt.Println("Найдены записи:")

			// Считываем и маршализируем контакты
			for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
				err := proto.Unmarshal(v, contact)
				Check(err)
				// Отображаем контакты через форматированный вывод
				PrintContact(string(k), contact)
			}
		}
		return nil
	}); err != nil {
		panic(err)
	}
}

func FindByNumber(number string) {
	// Открываем БД
	dbContacts, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer dbContacts.Close()

	// Открываем Read-only транзакцию
	if err := dbContacts.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(ContactsBucketName))
		if bucket == nil { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			printEmptyBucketMessage()
		} else {
			if contactTmp := bucket.Get([]byte(number)); contactTmp != nil { // Если номер есть в БД - выводим инфу о нем на дисплей
				contact := &models.Contact{}
				err := proto.Unmarshal(contactTmp, contact)
				Check(err)
				fmt.Println("Найдена запись:")
				// Отображаем контакт через форматированный вывод
				PrintContact(number, contact)
			} else { // Если номера нет в БД - сообщаем об этом и выходим
				fmt.Println("Запись с таким номером не существует")
			}
		}
		return nil
	}); err != nil {
		panic(err)
	}
}

func EditContact(number string, contact *models.Contact) bool {
	// Открываем БД
	dbContacts, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer dbContacts.Close()
	var flagToReturn bool

	// Открываем Writable транзакцию
	if err := dbContacts.Update(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactTmp := bucket.Get([]byte(number)); contactTmp != nil { // Проверяем наличие номера в БД
				contactFromDB := &models.Contact{}
				err := proto.Unmarshal(contactTmp, contactFromDB)
				Check(err)

				fmt.Println("Найдена запись:")
				PrintContact(number, contactFromDB) // Отображаем контакт из БД через форматированный вывод

				out, err := proto.Marshal(contact)
				Check(err)
				err = bucket.Put([]byte(number), out) // Перезаписываем контакт в БД
				Check(err)

				fmt.Println("Изменена на:")
				PrintContact(number, contact) // Отображаем контакт (записываемый в БД) через форматированный вывод

				flagToReturn = true
			} else { // Если номера нет в БД - сообщаем об этом и выходим
				fmt.Println("Запись с таким номером не существует")
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			printEmptyBucketMessage()
		}

		return nil
	}); err != nil {
		panic(err)
	}
	return flagToReturn
}

func DeleteContact(number string) bool {
	// Открываем БД
	dbContacts, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer dbContacts.Close()
	var flagToReturn bool

	// Открываем Writable транзакцию
	if err := dbContacts.Update(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactTmp := bucket.Get([]byte(number)); contactTmp != nil { // Проверяем наличие номера в БД
				contactFromDB := &models.Contact{}
				err := proto.Unmarshal(contactTmp, contactFromDB)
				Check(err)

				fmt.Println("Найдена запись:")
				PrintContact(number, contactFromDB) // Отображаем контакт из БД через форматированный вывод

				err = bucket.Delete([]byte(number)) // Удаляем запись с БД
				Check(err)
				flagToReturn = true
			} else { // Если номера нет в БД - сообщаем об этом и выходим
				fmt.Println("Запись с таким номером не существует")
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			printEmptyBucketMessage()
		}
		return nil
	}); err != nil {
		panic(err)
	}
	return flagToReturn
}

func printEmptyBucketMessage() {
	fmt.Println("Записи в базе данных пока отсутствуют. Можете добавить запись с помощью команды addContact")
}

func PrintContact(phone string, contact *models.Contact) {
	fmt.Println("contact {")
	fmt.Println("PHONE:", phone)
	fmt.Println("NAME:", contact.Name)
	fmt.Println("ADDRESS:", contact.Address)
	fmt.Println("}")
}

func CheckParamsExceptApartment(phone, name, city, street, building string) bool { // Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом
	if Validate(phone, PhoneFlag) && Validate(name, NameFlag) && Validate(city, CityFlag) && Validate(street, StreetFlag) && Validate(building, BuildingOrApartmentFlag) {
		return true
	}
	PrintValidationMessages()
	return false
}

func Validate(data string, flag string) bool {
	switch flag {
	case PhoneFlag:
		return PhoneRegexp.MatchString(data)
	case NameFlag:
		return NameRegexp.MatchString(data)
	case CityFlag:
		return CityRegexp.MatchString(data)
	case StreetFlag:
		return StreetRegexp.MatchString(data)
	case BuildingOrApartmentFlag:
		return BuildingOrApartmentRegexp.MatchString(data)
	default:
		return false
	}
}

func PrintValidationMessages() string {
	fmt.Println("Попробуйте еще раз. При этом введите верные данные")
	fmt.Println("Формат номера телефона +380xxYYYYYYY")
	fmt.Println("Имя - только буквы A-z и/или цифры (от 3 до 15 символов)")
	fmt.Println("Город - только буквы A-z (от 3 до 15 символов)")
	fmt.Println("Улица - только буквы A-z и/или цифры (от 4 до 20 символов)")
	fmt.Println("Дом/квартира - только буквы A-z и/или цифры (от 1 до 5 символов)")
	return "PrintValidationMessages()"
}

func Check(e error) {
	if e != nil {
		panic(e)
	}
}
