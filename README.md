# goProtobuf

GO-приложение, использующее технологии Cobra и Protobuf, а также базу данных BoltDB. Работает по типу телефонной 
(адресной) книги. Номер телефона является ключом - добавить две записи с одинаковыми номерами невозможно. 
Структура Contact является значением для ключа.

### Загрузка проекта
Использовать любую из команд:<br>
1 - **go get gitlab.com/oleg.kuleba/goBoltDB** с любой директории<br>
( если не сработает, то использовать комманду go get gitlab.com/oleg.kuleba/goBoltDB.git и после нее изменить
 название папки проекта с goBoltDB.git на goBoltDB )<br>
или<br>
2 - **git clone https://gitlab.com/oleg.kuleba/goBoltDB.git** в директории %GOPATH%\src\gitlab.com\oleg.kuleba

### Использование
Все команды используются в формате go run main.go {command} [arg1, arg2, ..., argN]
Применять команды необходимо, находясь в каталоге проекта.

### Формат команд
#### addContact
addContact номерТелефона имя город улица дом [квартира]<br>
пример: go run main.go addContact +380994445666 Taras Dnipro Titova 26a 45
#### findAll
findAll<br>
пример: go run main.go findAll
#### findByNumber
findByNumber номерТелефона<br>
пример: go run main.go findByNumber +380994445666
#### editContact
editContact номерТелефона имя город улица дом [квартира]<br>
пример: go run main.go editContact +380994445666 Taras Kyiv Shevchenka 15
#### deleteContact
deleteContact номерТелефона<br>
пример: go run main.go deleteContact +380994445666